#!/bin/sh

find -name "*.java" > sources.txt

javac $1 -d bin -classpath bin:lib/DepSkyDependencies.jar:lib/commons-io-1.4.jar:lib/PVSS.jar:lib/JReedSolEC.jar:lib/AmazonAccess.jar:lib/GoogleSAccess.jar:lib/RackAzureAccess.jar:lib/azureblob-1.5.4.jar:lib/azure-common-1.5.4.jar @sources.txt

rm sources.txt
