# Introduction

DepSky is a system that improves the availability, confidentiality and integrity of stored data in the cloud. It reaches this goal by encrypting, encoding and replicating all the data on a set of differents clouds, forming a cloud-of-clouds. For the current implementation of the system and for the text below we consider a cloud-of-clouds formed by four clouds.

More specifically DepSky address four important limitations:

* **Loss of availability** - DepSky addresses this limitation because it replicates all the data in a set of clouds, so even if some of them go down, all the data will be available if a subset of them are reachable.
* **Loss and corruption of data** - DepSky deals with this problem using Byzantine fault-tolerance replication to store data in a cloud-of-clouds, making it possible to get the data correctly even if some of the clouds corrupt or lose data.
* **Loss of privacy** - DepSky employs a secret sharing scheme and erasure codes to ensure that all data that will be stored in a cloud-of-clouds is in ciphertext.
* **Vendor lock-in** - DepSky addresses this limitation because instead of using a single cloud provider, you use a set of them.

# Protocols

Below is a brief explanation of the DepSky protocols to store data in a cloud-of-clouds. All of them replicate the data for all clouds used, but only the last three ensure that the data is properly stored (due to the Byzantines quorums).

### DepSky-A


This protocol replicates all the data in clear text in each cloud.

### DepSky-CA

This protocol uses secret sharing and erasure code techniques to replicate the data in a cloud-of-clouds. The image below shows how this is done. First an encryption key is generated, and then the original data block is encrypted. The encrypted data block is then erasure coded and key shares of the encryption key are computed. In this case we get four erasure coded blocks and four key shares because we use four clouds. Lastly, the data is stored in each cloud a different coded block together with a different key share.

### DepSky-only-JSS


This protocol only uses secret sharing. Basically, an encryption key is generated and the data is encrypted. Then four key shares of the key are created. Finally, the shares are spread to each cloud and each data segment is encrypted together with a different key share.

### DepSky-only-JEC


This protocol only uses erasure codes to replicate the data. The data is erasure coded in four different blocks and then each of them is stored in a different provider.

This protocol may be useful if your application has already encrypted the data.

# Costs


It might be expected that a DepSky client would be required to pay four (using a cloud-of-clouds of four cloud providers) times more than he would pay if uses a single cloud. This actually does not happen (if using DepSky-CA protocol) due to the erasure codes technique. The erasure codes technique used (see JEC) allow us to store in each of the four cloud providers only half of the orginal block data size. So, using DepSky, the client only will pay twice more than using a single cloud.

For more information see the DepSky paper. You can find it here [EuroSys'11 paper](http://www.di.fc.ul.pt/~bessani/publications/eurosys11-depsky.pdf)

# About The Program

First you need to create the accounts at Amazon S3 (http://aws.amazon.com/pt/s3/), RaskSpace (http://www.rackspace.co.uk/), Windows Azure (https://www.windowsazure.com/en-us/) and Google Storage (https://developers.google.com/storage/). After creating all necessary accounts, you need to fill the access credentials in the 'accounts.properties' file in the config folder.
If you only want to use Amazon S3 as your cloud storage provider, you can only create an account at Amazon S3 and use the example file provided (config/accounts_amazon.properties). To do that, copy the content of the 'accounts_amazon.properties' file to the one mentioned before (config/accounts.properties). In this case four diferent Amazon S3 locations will be used to store the data (US_Standard, EU_Ireland, US_West and AP_Tokyo).


To run DepSky on the command line just run the script DepSky_Run.sh passing 3 arguments: the id of the client; the protocol you want to use DepSky (0 for DepSky-A, 1 for DepSky-CA, 2 for use only erasure codes, and 3 for use only sercret sharing); and the the storage location (0 if you want to use cloud storage to replicate data and 1 if you want to run DepSky with local storage). The main implemented in LocalDepSkySClient.java is just for test. You can change it.


If you want to test DepSky storing all data and metadata localy, you need to run first the local server at /src/depskys/clouds/drivers/localStorageService/ServerThread.java. To do this just run the script Run_LocalStorage.sh. This server will receive all requests at ip 127.0.0.1 and port 5555.


# Building The Project

To build the source from the command line using javac, run the script compile.sh.
```bash
./compile.sh
```
To see the output of the compiling process, you can run the compile script with the -verbose option.
```bash
./compile.sh -verbose
```
